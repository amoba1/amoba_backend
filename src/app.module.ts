import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GameModule } from './api/game/game.module';
import { LobbyModule } from './api/lobby/lobby.module';
import PrismaModule from "./prisma/prisma.module";
import {ScheduleModule} from "@nestjs/schedule";

@Module({
  imports: [
    ScheduleModule.forRoot(),
      PrismaModule, GameModule, LobbyModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
