import {BadRequestException, Injectable} from '@nestjs/common';
import {PrismaService} from "../../prisma/prisma.service";
import { v4 as uuid } from 'uuid';
import {Prisma} from "@prisma/client";
import {Cron, CronExpression} from "@nestjs/schedule";

const rows: number = 10;
const columns: number = 20;
const initialGameBoard: string[][] = Array.from({ length: rows }, () => Array.from({ length: columns }, () => ''));

export interface LobbyResponse {
    sessionId: string;
    isFirstPlayerInTheLobby: boolean,
}


@Injectable()
export class LobbyService {

    constructor(private prismaService: PrismaService) {}

    async findMatch() {
        await this.deleteLobby();
        const sessionId = uuid();
        const players = await this.prismaService.lobby.findMany();
        let isFirstPlayerInTheLobby: boolean = false;
        if (players.length === 0) {
            try {
                let user: Prisma.lobbyCreateInput = {
                    sessionid: sessionId,
                }
                await this.prismaService.lobby.upsert({
                    where: {sessionid: sessionId},
                    update: {
                        sessionid: sessionId
                    },
                    create: user
                });
            } catch (e) {
                throw new BadRequestException('Failed to add you to the lobby!');
            }

        } else {
            const session1 = await this.prismaService.lobby.findMany({
                select: {
                    sessionid: true
                }
            });
            if (session1) {
                isFirstPlayerInTheLobby = true;
            }
            await this.prismaService.lobby.deleteMany();
            await this.prismaService.game.create({
                data: {
                    session1: session1[0].sessionid,
                    session2: sessionId,
                    state: JSON.stringify(initialGameBoard),
                    turn: session1[0].sessionid,
                }
            })
        }
        const response: LobbyResponse = {
            sessionId: sessionId,
            isFirstPlayerInTheLobby: isFirstPlayerInTheLobby,
        }
        return response;
    }

    @Cron(CronExpression.EVERY_10_SECONDS)
    async deleteLobby() {
        const timeNow = new Date().getTime();
        const lobby = await this.prismaService.lobby.findMany({});
        if (lobby.length !== 0) {
            const lobbyTime = new Date(lobby[0].timestamp).getTime();
            const differenceInMinutes = Math.abs((lobbyTime - timeNow) / (1000 * 60));

            if (differenceInMinutes >= 0.1) {
                await this.prismaService.lobby.deleteMany({});
            }
        }
    }
}
