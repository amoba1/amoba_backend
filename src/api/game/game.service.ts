import {Injectable} from '@nestjs/common';
import {PrismaService} from "../../prisma/prisma.service";
import {Coordinates} from "./game.controller";
import {Cron, CronExpression} from "@nestjs/schedule";

export interface GameStateResponse {
    state: JSON,
    firstPlayer: boolean,
    turn: string | null,
    winner: string | null,
}

@Injectable()
export class GameService {
    constructor(private prismaService: PrismaService) {}

    async gameState(sessionId: string) {
        const game = await this.prismaService.game.findMany({
            where: {
                OR: [{ session1: sessionId }, { session2: sessionId }]
            }
        });
        const response: GameStateResponse = {
            state: null,
            firstPlayer: game.length === 0,
            turn: game.length !== 0 ? game[0].turn : null,
            winner: null
        };
        if (game.length !== 0) {
            const jsonBoard: JSON = JSON.parse(game[0].state.toString());
            const boardMatrix: string[][] = JSON.parse(JSON.stringify(jsonBoard));
            response.state = jsonBoard;
            if (checkWinner(boardMatrix, 'X')) response.winner = game[0].session1;
            else if (checkWinner(boardMatrix, 'O')) response.winner = game[0].session2;
            else if (checkDraw(boardMatrix)) response.winner = "draw";
        } else {
            await this.prismaService.lobby.update({
                where: {
                    sessionid: sessionId,
                },
                data: {
                    timestamp: new Date().toISOString(),
                }
            })
        }
        return response;
    }


    async play(sessionId: string, coordinates: Coordinates) {
        const result = await this.gameState(sessionId);
        const board = result.state;
        const game = await this.prismaService.game.findMany({
            where: {
                OR: [{ session1: sessionId }, { session2: sessionId }]
            }
        })
        const nextTurn :string = game[0].turn === game[0].session1 ? game[0].session2 : game[0].session1;
        if (nextTurn === game[0].session1) {
            board[coordinates.x][coordinates.y] = 'O';
        } else {
            board[coordinates.x][coordinates.y] = 'X';
        }
        await this.prismaService.game.update({
            where: {
                id: game[0].id
            },
            data: {
                state: JSON.stringify(board),
                turn: nextTurn,
                lastActionTimestamp: new Date().toISOString(),
            }
        })
        return board;
    }

    @Cron(CronExpression.EVERY_MINUTE)
    async deleteGameAfterNoInteractions() {
        const timeNow = new Date().getTime();
        const games = await this.prismaService.game.findMany({});
        if (games.length !== 0) {
            for (const game of games) {
                const gameLastActionTime = game.lastActionTimestamp.getTime();
                const differenceInMinutes = Math.abs((gameLastActionTime - timeNow) / (1000 * 60));
                if (differenceInMinutes >= 2) {
                    await this.prismaService.game.deleteMany({
                        where: {
                            id: game.id,
                        }
                    });
                }
            }
        }
    }
}

function checkWinner(board: string[][], player: string): boolean {
    for (const row of board) {
        let count = 0;
        for (const cell of row) {
            if (cell === player) {
                count++;
                if (count === 5) {
                    return true;
                }
            } else {
                count = 0;
            }
        }
    }

    for (let col = 0; col < board[0].length; col++) {
        let count = 0;
        for (let row = 0; row < board.length; row++) {
            if (board[row][col] === player) {
                count++;
                if (count === 5) {
                    return true;
                }
            } else {
                count = 0;
            }
        }
    }

    for (let startRow = 0; startRow < board.length - 4; startRow++) {
        for (let startCol = 0; startCol < board[0].length - 4; startCol++) {
            let count = 0;
            for (let i = 0; i < 5; i++) {
                if (board[startRow + i][startCol + i] === player) {
                    count++;
                    if (count === 5) {
                        return true;
                    }
                } else {
                    count = 0;
                }
            }
        }
    }

    for (let startRow = 0; startRow < board.length - 4; startRow++) {
        for (let startCol = 4; startCol < board[0].length; startCol++) {
            let count = 0;
            for (let i = 0; i < 5; i++) {
                if (board[startRow + i][startCol - i] === player) {
                    count++;
                    if (count === 5) {
                        return true;
                    }
                } else {
                    count = 0;
                }
            }
        }
    }

    return false;
}

function checkDraw(board: string[][]) {
    for (let row of board) {
        for (let elem of row) {
            if (elem === "") {
                return false;
            }
        }
    }
    return true;
}