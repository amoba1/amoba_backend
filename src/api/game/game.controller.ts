import {Body, Controller, Get, Param, Post} from '@nestjs/common';
import {GameService, GameStateResponse} from "./game.service";

export interface Coordinates {
    x: number,
    y: number,
}

@Controller('game')
export class GameController {

    constructor(private readonly gameService: GameService) {}

    @Get('/:sessionId')
    gameState(@Param('sessionId') sessionId: string): Promise<GameStateResponse> {
        return this.gameService.gameState(sessionId);
    }

    @Post('/:sessionId/play')
    play(@Param('sessionId') sessionId: string, @Body() coordinates: { x: number, y: number }) {
        return this.gameService.play(sessionId, coordinates);
    }

}
