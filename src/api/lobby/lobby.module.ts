import { Module } from '@nestjs/common';
import { LobbyService } from './lobby.service';
import { LobbyController } from './lobby.controller';
import PrismaModule from "../../prisma/prisma.module";

@Module({
  imports: [PrismaModule],
  providers: [LobbyService],
  controllers: [LobbyController]
})
export class LobbyModule {}
