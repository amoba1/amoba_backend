import {Controller, Post} from '@nestjs/common';
import {LobbyResponse, LobbyService} from "./lobby.service";

@Controller('lobby')
export class LobbyController {

    constructor(private readonly lobbyService: LobbyService) {}


    @Post('/')
    findMatch(): Promise<LobbyResponse> {
        return this.lobbyService.findMatch();
    }
}
